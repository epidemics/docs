# eu vou mata o coronaviros (Precisamos de um nome!)

## Arquitetura em alto nível

![high_level](high_level.png)

## Arquitetura interna do aplicativo

![app](app.png)

Tecnologias:

- Android
- Kotlin


## Arquitetura do backend

![backend](backend.png)

Tecnologias:

- Go
- gRPC
- PostgreSQL
- Envoy Proxy
